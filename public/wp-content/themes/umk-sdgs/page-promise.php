<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="bg-promise pt-20 pt-md-44 mb-20 mb-md-40">
	<div class="container">
		<div class="promise-main-container bg-white px-4 pt-8 pt-md-20 pt-lg-40 pb-20 pb-md-40 pb-md-52">
			<img src="<?php echo get_template_directory_uri(); ?>/img/promise-main.png" alt="" class="d-block w-75 mx-auto mb-6 mb-md-20 promise-main-img">
			<p class="f-16 f-md-24 lh-16 mb-6 mb-md-12 font-weight-bold text-md-center">テレビ宮崎は、国連の気候変動対策キャンペーン「1.5℃の約束」に参加しています。</p>
			<p class="lh-24 f-md-16 mb-0 text-md-center">
				このキャンペーンは、国連『SDGメディア・コンパクト』に加盟する<br>
				日本のメディア有志が共同で実施するものです。<br>
				メディアの情報発信を通じて、<br>
				なぜ世界の平均気温上昇を産業革命以前と比較して<br>
				1.5℃に抑えることが必要なのかについて理解を促進し、<br>
				地球温暖化をはじめとする気候変動に歯止めをかけるための<br>
				具体的なアクションを提示し、個人や組織に行動変容を促すことを目的としています。
			</p>
		</div>
	</div>
</section>

<section class="promise-sec-act">
	<div class="container">
		<h2 class="f-18 f-md-40 font-weight-bold text-center promise-sec-title pb-2 mb-8 mb-md-12">個人でできる<span class="f-30 f-md-60">10</span>の行動</h2>
		<p class="mb-14 mb-md-20 f-md-16 text-md-center lh-24">
			誰もが気候変動の抑制に貢献できます。<br>
			移動手段から使用する電力、食べ物に至るまで、私たちは変化をもたらすことができるのです。<br>
			気候危機に立ち向かうために、以下の10の行動から始めましょう。
		</p>

		<div class="row no-gutters mx-n2 mx-md-n4 mx-lg-6">
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 01</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						家庭で節電する
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-01.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						私たちが使用する電力や熱の大部分は、石炭や石油、ガスを燃料としています。冷暖房の使用を控え、LED電球や省エネタイプの電化製品に取り替え、冷水で洗濯し、乾燥機を使わずに干して乾燥させてエネルギー消費量を減らしましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 02</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						徒歩や自転車で移動する、<br>
						または公共交通機関を利用する
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-02.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						世界中の道路が車であふれ返り、そのほとんどが軽油やガソリンを燃焼させています。自動車に乗る代わりに徒歩や自転車で移動すれば、温室効果ガスの排出が削減され、健康と体力の増進に役立ちます。移動距離が長いときは列車やバスの利用を検討してください。また、自動車は可能な限り相乗りで利用しましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 03</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						野菜をもっと多く食べる
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-03.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						野菜や果物、全粒穀物、豆類、ナッツ類、種子の摂取量を増やし、肉や乳製品を減らすと環境への影響を大幅に軽減できます。一般に、植物性食品の生産による温室効果ガスの排出はより少なく、必要なエネルギーや土地、水の量も少なくなります。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 04</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						長距離の移動手段を考える
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-04.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						飛行機は大量の化石燃料を燃やし、相当量の温室効果ガスを排出します。つまり、飛行機の利用を減らすことは、環境への影響を軽減する最も手っ取り早い方法の一つです。可能な限りオンラインで会ったり、列車を利用したり、長距離移動そのものを止めたりしましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 05</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						廃棄食品を減らす
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-05.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						食料を廃棄すると、食料の生産、加工、梱包、輸送のために使った資源やエネルギーも無駄になります。また、埋め立て地で食品が腐敗すると、強力な温室効果ガスの一種であるメタンガスが発生します。購入した食品は使い切り、食べ残しはすべて堆肥にしましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 06</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						リデュース、リユース、<br>
						リペア、リサイクル
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-06.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						私たちが購入する電子機器や衣類などは、原材料の抽出から製品の製造、市場への輸送まで、生産の各時点で炭素を排出します。買う物を減らし、中古品を購入し、修理できるものは修理し、リサイクルして地球の気候を守りましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 07</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						家庭のエネルギー源をかえる
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-07.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						自宅のエネルギー源が石油、石炭、ガスのどれなのかを電力会社に確認しましょう。可能なら、風力や太陽光などの再生可能エネルギー源への切り替えができるかどうかも確かめてください。あるいは、自宅の屋根にソーラーパネルを設置して家庭で使用する電力を賄いましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 08</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						電気自動車にのりかえる
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-08.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						自動車の購入を予定しているなら、電気自動車を検討してください。より安価なモデルが市場にますます多く出回っています。化石燃料から作られた電力で走行するにしても、電気自動車はガソリン車やディーゼル車より大気汚染の軽減に役立ち、温室効果ガスの排出量が大幅に削減されます。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 09</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						環境に配慮した製品を選ぶ
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-09.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						私たちが購入するあらゆるものが地球に影響を及ぼします。あなたには、どのような商品やサービスを支持するかを選択する力があります。自身が環境に及ぼす影響を軽減するために、地元の食品や旬の食材を購入し、責任を持って資源を使ったり、温室効果ガス排出や廃棄物の削減に力を入れていたりしている企業の製品を選びましょう。
					</p>
				</div>
			</div>
			<div class="col-12 col-md-6 px-2 px-md-4 px-lg-6 mb-10 mb-md-20 align-self-stretch">
				<div class="act-box h-100 px-4 px-md-6 py-6 pt-md-14 pb-md-10">
					<p class="font-weight-bold text-center act-number f-18 f-md-22 pb-2">ACT 10</p>
					<h3 class="text-center f-18 f-md-22 lh-16 font-weight-bold mb-4 mb-md-8 act-box-title">
						声を上げる
					</h3>
					<img src="<?php echo get_template_directory_uri(); ?>/img/promise-act-10.png" alt="" class="d-block w-100 mx-auto mb-4 mb-md-10">
					<p class="mb-0 act-box-text">
						声を上げて、他の人たちにも行動に参加してもらいましょう。声を上げることが、変化をもたらす最も手っ取り早く、最も効果的な方法の一つです。あなたの隣人や同僚、友人、家族と話してください。経営者には、あなたが大胆な変革を支持することを伝えましょう。地域や世界のリーダーたちに、今こそ行動を起こすように訴えましょう。
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	const heightLine = function() {
		const titleArr = document.querySelectorAll('.act-box-title');
		const textArr = document.querySelectorAll('.act-box-text');

		const titleHeightArr = [];
		const textHeightArr = [];

		for (let i = 0; i < titleArr.length; i++) {
			titleHeightArr.push(titleArr[i].clientHeight);
		}

		for (let i = 0; i < textArr.length; i++) {
			textHeightArr.push(textArr[i].clientHeight);
		}

		const maxTitleHeight = Math.max.apply(null, titleHeightArr);
		const maxTextHeight = Math.max.apply(null, textHeightArr);

		for (let i = 0; i < titleArr.length; i++) {
			titleArr[i].style.height = maxTitleHeight + 'px';
		}

		for (let i = 0; i < textArr.length; i++) {
			textArr[i].style.height = maxTextHeight + 'px';
		}
	}

	window.addEventListener('load', function() {
		setTimeout(function() {
			heightLine();
		}, 300)
	})
</script>

<?php
get_footer();
