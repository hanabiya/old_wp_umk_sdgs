<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package umk-sdgs
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-24692597-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-24692597-1');
	</script>


	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&display=swap" rel="stylesheet">
	<script>
		(function(d) {
			var config = {
					kitId: 'uam5xkm',
					scriptTimeout: 3000,
					async: true
				},
				h = d.documentElement,
				t = setTimeout(function() {
					h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
				}, config.scriptTimeout),
				tk = d.createElement("script"),
				f = false,
				s = d.getElementsByTagName("script")[0],
				a;
			h.className += " wf-loading";
			tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
			tk.async = true;
			tk.onload = tk.onreadystatechange = function() {
				a = this.readyState;
				if (f || a && a != "complete" && a != "loaded") return;
				f = true;
				clearTimeout(t);
				try {
					Typekit.load(config)
				} catch (e) {}
			};
			s.parentNode.insertBefore(tk, s)
		})(document);
	</script>


	<?php wp_head(); ?>
</head>

<body id="body">

	<header class="header-pos header">
		<?php if (!(is_page('municipal-week') || is_single())) : ?>
			<div class="container-fluid px-0">
				<img src="<?php echo get_template_directory_uri(); ?>/img/borderline.png" alt="" class="w-100 bg-white d-none d-md-block m-0">
				<img src="<?php echo get_template_directory_uri(); ?>/img/borderline-sp.png" alt="" class="w-100 bg-white d-block d-md-none m-0">
				<div class="row no-gutters align-items-center pt-6 pt-xl-3 pb-xl-5 px-5 px-md-5 gradient">
					<div class="col-12 px-0 d-none d-lg-block">
						<ul class="nav d-flex pc-menu justify-content-end align-items-center mb-0 py-3 pl-0 pr-3 mx-0 font-weight-bold">
							<li class="lsn">
								<a href="<?php echo home_url('/') ?>/" class="lsn-text js-text-color mont">HOME</a>
							</li>
							<li class="lsn">
								<a href="<?php echo home_url('/') ?>future/" class="lsn-text js-text-color mont">喜びと笑顔あふれる未来へ</a>
							</li>
							<li class="lsn">
								<a href="<?php echo home_url('/') ?>contribution/" class="lsn-text js-text-color mont">地域・社会への貢献</a>
							</li>
							<li class="lsn">
								<a href="<?php echo home_url('/') ?>balance/" class="lsn-text js-text-color mont">ワークライフバランスの<br class="d-block d-md-none">実践と提案</a>
							</li>
							<li class="lsn">
								<a href="<?php echo home_url('/') ?>conservation/" class="lsn-text js-text-color mont">地域環境保全のために</a>
							</li>
						</ul>
					</div>
					<div class="col-12">
						<div class="menu-trigger sp-menu ml-auto mr-0 d-lg-none d-block">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
					<div class="menu-slide">
						<ul class="mx-0 px-0 text-center">
							<li class="lsn border-bottom border-blue">
								<a href="<?php echo home_url('/') ?>" class="tdn js-text-color mont text-blue">HOME</a>
							</li>
							<li class="lsn border-bottom border-blue">
								<a href="<?php echo home_url('/') ?>future/" class="tdn js-text-color mont text-blue">喜びと笑顔あふれる未来へ</a>
							</li>
							<li class="lsn border-bottom border-blue">
								<a href="<?php echo home_url('/') ?>contribution/" class="tdn js-text-color mont text-blue">地域・社会への貢献</a>
							</li>
							<li class="lsn border-bottom border-blue">
								<a href="<?php echo home_url('/') ?>balance/" class="tdn js-text-color mont text-blue">ワークライフバランスの<br class="d-block d-md-none">実践と提案</a>
							</li>
							<li class="lsn border-bottom border-blue">
								<a href="<?php echo home_url('/') ?>conservation/" class="tdn js-text-color mont text-blue">地域環境保全のために</a>
							</li>
						</ul>
					</div>
					<div id="" class="menu-trigger close-btn"></div>
				</div>
			</div>
		<?php else : ?>
			<div class="container-fluid px-0">
				<img src="<?php echo get_template_directory_uri(); ?>/img/borderline.png" alt="" class="w-100 bg-white d-none d-md-block m-0">
				<img src="<?php echo get_template_directory_uri(); ?>/img/borderline-sp.png" alt="" class="w-100 bg-white d-block d-md-none m-0">
				<div class="row no-gutters align-items-center pt-6 pt-xl-3 pb-xl-5 px-5 px-md-5 gradient">
					<div class="col-12 px-0 d-none d-lg-block">
						<ul class="nav d-flex pc-menu justify-content-end align-items-center mb-0 py-3 pl-0 pr-3 mx-0 font-weight-bold">
							<li class="lsn">
								<?php
								$pageFlag = is_page('municipal-week');
								if ($pageFlag) :
								?>
									<a href="#schedule" class="js-smooth lsn-text js-text-color mont">
										放送予定
									</a>
								<?php else : ?>
									<a href="<?php echo home_url('/'); ?>municipal-week/#schedule" class="lsn-text js-text-color mont">
										放送予定
									</a>
								<?php endif; ?>
							</li>
							<li class="lsn">
								<a href="<?php echo home_url('/'); ?>" target="_blank" class="lsn-text js-text-color mont">
									テレビ宮崎 SDGsページ
								</a>
							</li>
						</ul>
					</div>
					<div class="col-12">
						<div class="menu-trigger sp-menu ml-auto mr-0 d-lg-none d-block">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
					<div class="menu-slide">
						<ul class="mx-0 px-0 text-center">
							<li class="lsn border-bottom border-blue">
								<?php
								if ($pageFlag) :
								?>
									<a href="#schedule" class="js-smooth tdn js-text-color mont text-blue">
										放送予定
									</a>
								<?php else : ?>
									<a href="<?php echo home_url('/'); ?>municipal-week/#schedule" class="tdn js-text-color mont text-blue">
										放送予定
									</a>
								<?php endif; ?>

							</li>
							<li class="lsn border-bottom border-blue">
								<a href="<?php echo home_url('/'); ?>" class="tdn js-text-color mont text-blue">
									テレビ宮崎 SDGsページ
								</a>
							</li>
						</ul>
					</div>
					<div id="" class="menu-trigger close-btn"></div>
				</div>
			</div>
		<?php endif; ?>

	</header>
