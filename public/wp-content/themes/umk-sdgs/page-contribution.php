<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="container-fluid bg-mv-contribution header-m h-fv mv-img-border-bottom">
  <div class="container">
    <div class="row">
    </div>
  </div>
</section>

<section class="container-fluid mt-md-10 mb-md-10">
  <div class="container">
    <div class="row text-center position-relative">
      <div class="col-12 pt-10 pb-5">
        <h3 class="sec-title text-blue mb-8 font-noto-bold">地域・社会への貢献</h3>
        <p class="text-left text-md-center">「伝える」・「発信する」メディアとしての強みを活かし、<br class="d-none d-md-block">
          誰もが安心して住み続けられる宮崎の街づくりへ貢献していきます。

        </p>
      </div>
      <div class="col-12 pb-10 mx-auto">
        <div class="d-flex future-mark justify-content-center flex-wrap w-100 mb-2">
          <div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-01.png" class="w-100" alt=""></div>
          <div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-02.png" class="w-100" alt=""></div>
          <div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-09.png" class="w-100" alt=""></div>
          <div class="z-future px-1 d-none d-md-block"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-11.png" class="w-100" alt=""></div>
          <div class="z-future px-1 d-none d-md-block"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
        </div>
        <div class="d-flex future-mark justify-content-center flex-wrap w-100">
          <div class="z-future px-1 d-block d-md-none"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-11.png" class="w-100" alt=""></div>
          <div class="z-future px-1 d-block d-md-none"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="container-fluid bg-Lgray py-15 position-relative">
  <div class="container">
    <div class="row">
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-lg-6 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-contribution-01.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-lg-6 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">ニュース・自社制作番組</p>
            <p class="future-detail">
              放送をはじめとするメディアを通じてSDGsの活動と普及などにつながる情報や地域の活動を広く県民に紹介していきます。
            </p>
            <div class="text-center text-lg-left"><a href="https://www.umk.co.jp/program/" class="d-inline-block more-btn" target="_blank">詳細を見る</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-lg-6 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-contribution-02.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-lg-6 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">防災パートナーシップ協定</p>
            <p class="future-detail">
              宮崎県内26市町村と防災に関する協定を締結し、災害時は、命を守るための連携・協力を図ります。締結により、平常時における災害予防対策の強化につなげ、災害に強い街をめざします。また、UMKアプリを活用し、災害時に必要な情報を携帯電話等の端末に届けることで減災へつなげます。
            </p>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-lg-6 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-contribution-03.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-lg-6 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">26市町村みやざき元気プロジェクト</p>
            <p class="future-detail">
              1つの市町村にスポットをあて、ニュースや自社制作番組等で1週間にわたり深く取り上げていきます。また、様々なイベント等を実施し、地域の賑わい創出と活性化につなげます。
            </p>
            <div class="text-center text-lg-left">
              <a href="<?php echo home_url('/') ?>municipal-week/" class="d-inline-block more-btn">詳細を見る</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center position-relative">
          <div class="col-12 col-lg-6 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-contribution-04.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-lg-6 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">オンライン社内見学</p>
            <p class="future-detail">
              関係団体と連携し、小学生を対象にオンラインでテレビの仕組みについて学べる取り組みを整え、子どもたちの学習支援に役立てます。
            </p>
          </div>
        </div>
      </div>
      <div class="pos-conservation-u">
        <img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="">
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
