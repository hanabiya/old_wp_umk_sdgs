<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="container-fluid bg-mv-sdgs-declaration h-fv header-m mv-img-border-bottom">
  <div class="container">
    <div class="row">
    </div>
  </div>
</section>

<section class="container-fluid mt-md-10 mb-md-10">
  <div class="container">
    <div class="row text-center position-relative">
      <div class="col-12 pt-10 pb-5">
        <h3 class="text-blue mb-8 font-noto-bold title-declaration px-md-10">テレビ宮崎 SDGs宣言</h3>
        <p class="text-center text-blue sub-title-declaration mb-0 font-noto-bold">
          子どもの未来のために<br>
          今 私たちができることを
        </p>
      </div>
    </div>
  </div>
</section>

<section class="container-fluid pb-15">
  <div class="container">
    <div class="row bg-blue position-relative">
      <div class="col-12">
        <p class="text-gray mb-0 p-4 py-md-16 px-md-20 px-lg-28 read-declaration" style="font-weight: 500; letter-spacing: 0.5px;">
          テレビ宮崎は、 ２０３０年の「ＳＤＧｓ（持続可能な開発目標）」の達成に向けた取り組みを推進し「ＳＤＧｓ宣言」を行いました。<br>
          放送、事業を通じて地域社会の課題や地域経済の発展を図ることで持続可能な社会の実現に貢献してまいります。また、テレビ番組などのコンテンツを通じて、ＳＤＧｓの内容や地域の取り組みを広く紹介してまいります。<br>
          そして何より、子どもたちが希望を抱き、夢を持ち続ける世界を作るための課題認識を持ち、地域の皆さまとともに、その解決に向けた活動を「子どもの未来のために」取り組んでまいります。<br>
          昨年、開局５０周年を迎えたテレビ宮崎が考える「ＮＥＸＴ５０（これからの５０年）」は、世界の目標である「ＳＤＧｓ」の共通理念である「誰一人取り残さない」と同じく、県民一人一人に寄り添うとの思いが込められております。<br>
          ＹＯＵ&ＵＭＫの “ＹＯＵ”とは宮崎県のみなさんであり、ＵＭＫに関係してくれているすべての人、そして子どもたち。<br>
          あなたの声に耳を傾け、さまざまなチャレンジをしてまいります。
        </p>
      </div>
      <div class="pos-declaration-u">
        <img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="">
      </div>
    </div>
  </div>
</section>


</div>
</section>

<?php
get_footer();
