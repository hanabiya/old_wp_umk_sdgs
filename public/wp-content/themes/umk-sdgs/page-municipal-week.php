<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="bg-mv-week header-m mv-img-border-bottom">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-miyazaki-genki-project.png" alt="" class="d-block pos-week-logo">
	<a href="<?php echo home_url('/'); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/img/umk-sdgs-logo.png" alt="" class="d-block w-100 pos-sdgs-logo">
	</a>
</section>

<section class="my-15 my-md-20">
	<div class="container">
		<div class="row text-center position-relative">
			<div class="col-12 mb-5 mb-md-12 z-10">
				<p class="text-left text-md-center fuchidori f-md-16">
					1つの市町村にスポットをあて、ニュースや自社制作番組等で1週間にわたり深く取り上げていきます。<br>
					また、様々なイベント等を実施し、地域の賑わい創出と活性化につなげます。
				</p>
			</div>

			<div class="col-12 text-center z-10">
				<div class="d-inline-block">
					<div class="date-content text-center mb-6 mb-md-10">
						<p class=" mb-0 f-20 f-md-28 font-weight-bold py-2 px-4">
							<?php echo get_field('day_start'); ?><br class="d-sm-none">〜<br class="d-sm-none"><?php echo get_field('day_end'); ?>
						</p>
					</div>
					<h4 class="f-18 f-md-28 font-weight-bold mb-6 mb-md-8">
						＼ <?php echo get_field('section_title'); ?> ／
					</h4>
					<div class="weekly-area font-weight-bold f-42 f-md-60 py-4 py-8 bg-white">
						<?php echo get_field('target_municipal'); ?>
					</div>
				</div>
			</div>

			<div class="bg-futureU"><img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="" class="w-100"></div>
		</div>
	</div>
</section>

<?php
// スタンプラリーがあるときだけ管理画面から表示・非表示を切り替えれるようにしています。
$stamprally_section_bool = get_field('section_stamprally_ctrl');
if ($stamprally_section_bool) :
?>
	<section id="present" class="bg-blue py-15 py-md-20">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9">
					<?php echo get_the_content(); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<section id="schedule" class="py-15 py-md-20">
	<div class="container">

		<div class="row">

			<div class="col-12 text-center">
				<div class="">
					<h3 class="sec-title text-blue font-noto-bold mb-8 mb-md-10">放送予定</h3>
				</div>
				<div class="date-content d-inline-block mb-6 mb-md-10">
					<p class=" mb-0 f-20 font-weight-bold py-2 px-4">
						<?php echo get_field('day_start'); ?><br class="d-sm-none">〜<br class="d-sm-none"><?php echo get_field('day_end'); ?>
					</p>
				</div>

				<?php
				// タブの情報
				$scheduleArr = [
					// ['帯番組', 'program'],
					['月曜', 'mon'],
					['火曜', 'tue'],
					['水曜', 'wed'],
					['木曜', 'thu'],
					['金曜', 'fri'],
					['土曜', 'sat'],
					// ['日曜', 'sun'],
				];
				$scheduleSelect = 'false';
				$scheduleSelectActive = '';
				?>

				<ul class="row no-gutters mx-n1 nav nav-pills mb-4 mb-md-8" id="pills-tab" role="tablist">
					<?php
					// ループでタブ出力
					for ($i = 0; $i < count($scheduleArr); $i++) :
						if ($scheduleArr[$i][1] === 'mon') :
							$scheduleSelect = 'true';
							$scheduleSelectActive = 'active';
						else :
							$scheduleSelect = 'false';
							$scheduleSelectActive = '';
						endif;

						echo '<li class="col-3 col-md nav-item px-1">';
						echo '<a class="d-block rounded-pill mb-2 btn-gray-to-blue f-12 f-sm-14 f-lg-16 font-weight-bold py-1 py-md-2 px-0 nav-link ' . $scheduleSelectActive . '" id="' . $scheduleArr[$i][1] . '" data-toggle="pill" href="#schedule-' . $i . '" role="tab" aria-controls="schedule-' . $i . '" aria-selected="' . $scheduleSelect . '">';
						echo $scheduleArr[$i][0];
						echo '</a>';
						echo '</li>';
					endfor;
					?>
				</ul>
				<div class="row tab-content" id="pills-tabContent">
					<?php
					// ループでカスタム投稿を出力
					for ($i = 0; $i < count($scheduleArr); $i++) :
						if ($scheduleArr[$i][1] === 'mon') :
							$scheduleSelect = 'true';
							$scheduleSelectActive = 'show active';
						else :
							$scheduleSelect = 'false';
							$scheduleSelectActive = '';
						endif;

						echo '<div class="col-12 tab-pane fade ' . $scheduleSelectActive . '" id="schedule-' . $i . '" role="tabpanel" aria-labelledby="' . $scheduleArr[$i][1] . '">';
					?>

						<?php
						$args = array(
							'post_type' => 'schedules', // 投稿タイプのスラッグを指定
							'post_status' => 'publish', // 公開済の投稿を指定
							'posts_per_page' => -1, // 投稿件数の指定
							'tax_query' => array(
								array(
									'taxonomy' => 'day_of_the_week', //カスタムタクソノミー
									'field' => 'slug',
									'terms' => $scheduleArr[$i][1], //タクソノミーターム
								)
							)
						);
						$the_query = new WP_Query($args);
						if ($the_query->have_posts()) :
						?>
							<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
								<?php
								$schedule_img = get_field('schedule_img');
								$schedule_date = get_field('schedule_date');
								$schedule_contents = get_field('schedule_contents');
								?>


								<div class="news-content d-lg-flex">
									<div class="news-img-area">
										<img src="<?php echo $schedule_img['url']; ?>" alt="" class="d-block w-100">
									</div>
									<div class="news-detiel-area text-left px-4 px-xl-6 py-6">
										<h3 class="text-blue f-20 font-weight-bold mb-4"><?php echo get_the_title(); ?></h3>
										<dl>
											<dt class="f-14"><span>放送日時</span></dt>
											<dd class="f-14 pl-4">
												<?php echo $schedule_date; ?>
											</dd>
										</dl>
										<dl>
											<dt class="f-14"><span>放送内容</span></dt>
											<dd class="f-14 pl-4">
												<?php echo $schedule_contents; ?>
											</dd>
										</dl>
									</div>
								</div>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						<?php else : ?>
							<p>この曜日に放送している番組は現在ございません。</p>
						<?php endif; ?>

					<?php
						echo '</div>';
					endfor;
					?>
				</div>
			</div>

		</div>

	</div>
</section>

<section id="cm" class="bg-blue py-15 py-md-20">
	<div class="container position-relative">

		<div class="row">
			<div class="col-12 mb-8 mb-md-10 text-center">
				<h3 class="sec-title text-blue font-noto-bold">
					市町村ウィーク<br class="d-md-none">みんなのCM
				</h3>
			</div>
		</div>

		<!-- 検索エリア -->
		<div class="row w-100 mb-8 mb-md-14 mx-auto pt-4 pt-md-8 data-search-container">
			<div class="col-12 d-flex justify-content-between align-items-center mb-4 mb-md-8 data-search-head">
				<p class="m-0 f-24 font-weight-bold">年月を指定する</p>
				<div class="data-search-button">
					<span></span>
					<span></span>
				</div>
			</div>

			<div class="col-12 data-search-area">
				<div class="mb-4">
					<p class="mb-2 font-weight-bold">- 年を選択してください</p>
					<ul class="row w-100 m-0 p-0">
						<li v-for="year in years" @click="inputYear(year)" class="col-6 col-md-3 nav-item px-1">
							<button type="button" :class="['w-100 rounded-pill btn-white-to-blue mb-2 text-center font-weight-bold py-1 py-2', selected.year === year ? 'active' : '']">{{year}}年</button>
						</li>
					</ul>
				</div>

				<div class="mb-4 mb-md-8">
					<p class="mb-2 font-weight-bold">- 月を選択してください</p>
					<ul class="row w-100 m-0 p-0">
						<li v-for="i in 12" @click="inputMonth(i)" class="col-3 col-md-2 nav-item mb-2 px-1">
							<button type="button" :class="['w-100 rounded-pill btn-white-to-blue mb-2 text-center font-weight-bold py-1 py-2', selected.month === i ? 'active' : '']">{{ i }}月</button>
						</li>
					</ul>
				</div>

				<div class="text-center mb-4 mb-md-8">
					<button type="button" @click="changeCM()" class="rounded-pill btn-white-to-blue font-weight-bold py-1 py-2 px-8 px-md-10">検索</button>
				</div>
			</div>
		</div>

		<div v-if="isLoading">
			<div class="loading"></div>
			<p class="text-center">Loading...</p>
		</div>
		<div v-else>
			<div v-if="postCM.length !== 0" class="tab-content" id="pills-tabContent">
				<div class="text-center mb-6 mb-md-8">
					<h4 class="f-20 f-md-24 font-weight-bold text-blue">{{ current.year }}年{{ current.month }}月のCM</h4>
				</div>
				<div class="row">
					<div v-for="post in postCM" class="col-12 col-sm-6 col-lg-4 mb-4">
						<div class="video fCol3 fCol2Sp">
							<div :id="getYoutubeId(post.acf.cm_link)" @click="setCM(post)" class="cm-modal-btn" data-toggle="modal" data-target=".weekly-cm" style="cursor: pointer;">
								<div class="position-relative">
									<img :src="`https://img.youtube.com/vi/${getYoutubeId(post.acf.cm_link)}/mqdefault.jpg`" alt="" class="d-block w-100 mb-3">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icon-start.png" alt="" class="d-block movie-icon">
								</div>
								<p class="text-black f-14 lh-16 mb-2">
									{{ post.title.rendered }}
								</p>
							</div>
							<p class="sns">
								<a :href="`https://twitter.com/intent/tweet?url=${ post.acf.cm_link }&amp;text=【UMK50thみんなのCM】${ post.title.rendered }`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconTW.png" alt="Twitter">
								</a>
								<a :href="`https://www.facebook.com/sharer.php?src=bm&amp;u=${ post.acf.cm_link }`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconFB.png" alt="Facebook">
								</a>
								<a :href="`https://timeline.line.me/social-plugin/share?url=${ post.acf.cm_link }`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconLN.png" alt="LINE">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div v-if="postCM.length === 0" class="text-center">
				<h4 class="f-20 font-weight-bold">{{ current.year }}年{{ current.month }}月のCMは見つかりませんでした。</h4>
			</div>
		</div>

	</div>

	<div v-if="currentCM.length !== 0" class="modal fade weekly-cm close" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content border-0 position-relative">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<div class="youtube-frame">
					<iframe id="youtubeModal" width="560" height="315" :src="`https://www.youtube.com/embed/${getYoutubeId(currentCM.acf.cm_link)}`" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="archive" class="py-15 py-md-20">
	<div class="container position-relative">

		<div class="row">
			<div class="col-12 mb-8 mb-md-10 text-center">
				<h3 class="sec-title text-blue font-noto-bold">
					放送アーカイブ
				</h3>
			</div>
		</div>

		<div class="row w-100 mb-8 mb-md-14 mx-auto pt-4 pt-md-8 data-search-container">
			<div class="col-12 d-flex justify-content-between align-items-center mb-4 mb-md-8 data-search-head">
				<p class="m-0 f-24 font-weight-bold">年月を指定する</p>
				<div class="data-search-button">
					<span></span>
					<span></span>
				</div>
			</div>

			<div class="col-12 data-search-area">
				<div class="mb-4">
					<p class="mb-2 font-weight-bold">- 年を選択してください</p>
					<ul class="row w-100 m-0 p-0">
						<li v-for="year in years" @click="inputYear(year)" class="col-6 col-md-3 nav-item px-1">
							<button type="button" :class="['w-100 rounded-pill btn-white-to-blue mb-2 text-center font-weight-bold py-1 py-2', selected.year === year ? 'active' : '']">{{year}}年</button>
						</li>
					</ul>
				</div>

				<div class="mb-4 mb-md-8">
					<p class="mb-2 font-weight-bold">- 月を選択してください</p>
					<ul class="row w-100 m-0 p-0">
						<li v-for="i in 12" @click="inputMonth(i)" class="col-3 col-md-2 nav-item mb-2 px-1">
							<button type="button" :class="['w-100 rounded-pill btn-white-to-blue mb-2 text-center font-weight-bold py-1 py-2', selected.month === i ? 'active' : '']">{{ i }}月</button>
						</li>
					</ul>
				</div>

				<div class="text-center mb-4 mb-md-8">
					<button type="button" @click="changeCM()" class="rounded-pill btn-white-to-blue font-weight-bold py-1 py-2 px-8 px-md-10">検索</button>
				</div>
			</div>
		</div>

		<div v-if="isLoading">
			<div class="loading"></div>
			<p class="text-center">Loading...</p>
		</div>
		<div v-else>
			<div v-if="postCM.length !== 0" class="tab-content" id="pills-tabContent">
				<div class="text-center mb-6 mb-md-8">
					<h4 class="f-20 f-md-24 font-weight-bold text-blue">{{ current.year }}年{{ current.month }}月の放送アーカイブ</h4>
				</div>
				<div class="row">
					<div v-for="post in postCM" class="col-12 col-sm-6 col-lg-4 mb-4">
						<div v-if="post.acf.movie_or_blog.value === 'movie'" class="video fCol3 fCol2Sp">
							<div :id="getYoutubeId(post.acf.cm_link)" @click="setCM(post)" class="cm-modal-btn" data-toggle="modal" data-target=".archive-cm" style="cursor: pointer;">
								<div class="position-relative">
									<img :src="`https://img.youtube.com/vi/${getYoutubeId(post.acf.cm_link)}/mqdefault.jpg`" alt="" class="d-block w-100 mb-3">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icon-start.png" alt="" class="d-block movie-icon">
								</div>
								<p class="text-black f-14 lh-16 mb-2">
									{{ post.title.rendered }}
								</p>
							</div>
							<p class="sns">
								<a :href="`https://twitter.com/intent/tweet?url=${ post.acf.cm_link }&amp;text=【UMK50thみんなのCM】`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconTW.png" alt="Twitter">
								</a>
								<a :href="`https://www.facebook.com/sharer.php?src=bm&amp;u=${ post.acf.cm_link }`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconFB.png" alt="Facebook">
								</a>
								<a :href="`https://timeline.line.me/social-plugin/share?url=${ post.acf.cm_link }`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconLN.png" alt="LINE">
								</a>
							</p>
						</div>

						<div v-if="post.acf.movie_or_blog.value === 'blog'" class="video fCol3 fCol2Sp">
							<a class="cm-modal-btn" :href="post.link">
								<div class="position-relative">
									<img :src="post['_embedded']['wp:featuredmedia'][0].source_url" alt="" class="d-block w-100 mb-3">
								</div>
								<p class="text-black f-14 lh-16 mb-2">
									{{ post.title.rendered }}
								</p>
							</a>
							<p class="sns">
								<a :href="`https://twitter.com/intent/tweet?url=${post.link}&amp;text=【UMK50thみんなのCM】${post.title.rendered}`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconTW.png" alt="Twitter">
								</a>
								<a :href="`https://www.facebook.com/sharer.php?src=bm&amp;u=${post.link}`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconFB.png" alt="Facebook">
								</a>
								<a :href="`https://timeline.line.me/social-plugin/share?url=${post.link}`" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/img/iconLN.png" alt="LINE">
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>

			<div v-if="postCM.length === 0" class="text-center">
				<h4 class="f-20 font-weight-bold">{{ current.year }}年{{ current.month }}月の放送アーカイブは見つかりませんでした。</h4>
			</div>
		</div>

		<div class="bg-u-02"><img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="" class="w-100"></div>
	</div>

	<div v-if="currentCM.length !== 0" class="modal fade archive-cm close" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content border-0 position-relative">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<div class="youtube-frame">
					<iframe id="youtubeModal" width="560" height="315" :src="`https://www.youtube.com/embed/${getYoutubeId(currentCM.acf.cm_link)}`" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
