<?php

/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="bg-mv-week-single header-m mb-10 mb-md-16">
	<a href="<?php echo home_url('/'); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/img/umk-sdgs-logo.png" alt="" class="d-block w-100 pos-sdgs-logo">
	</a>
</section>

<section class="container position-relative">
	<?php
	if (have_posts()) :
		while (have_posts()) :
			the_post();
	?>

			<div class="row mb-6 mb-md-10">
				<div class="col-12">
					<div class="border-color-blue border-bottom border-width-3 pb-2">
						<h2 class="f-24 f-md-30 font-weight-bold text-blue"><?php echo get_the_title(); ?></h2>
					</div>
				</div>
			</div>

			<div class="row position-relative" style="z-index:25;">
				<div class="col-12 mb-10 mb-md-16">
					<?php echo get_the_content(); ?>
				</div>
				<div class="col-12 text-center">
					<a href="<?php echo home_url('/'); ?>municipal-week" class="bgleft d-inline-block more-btn f-16 font-weight-bold px-10 py-2 rounded-pill"><span>TOPに戻る</span></a>
				</div>
			</div>

	<?php
		endwhile;
	endif;
	?>

	<div class="bg-u-02"><img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="" class="w-100"></div>
</section>


<?php
get_footer();
