<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="container-fluid bg-mv-balance header-m h-fv mv-img-border-bottom">
  <div class="container">
    <div class="row">
    </div>
  </div>
</section>

<section class="container-fluid mt-md-10 mb-md-10">
  <div class="container">
    <div class="row text-center position-relative">
      <div class="col-12 pt-10 pb-5 z-10">
        <h3 class="sec-title text-blue mb-8 font-noto-bold">ワークライフバランスの<br class="d-block d-md-none">実践と提案</h3>
        <p class="text-left text-md-center fuchidori">私たち自身が心身共に健康で、一人一人がいきいきと働ける環境を整え、実践し、<br>
          メディアとして県民の皆さんに広く発信していきたいと考えています。
        </p>
      </div>
      <div class="col-12 pb-10 z-10">
        <div class="d-flex future-mark justify-content-center flex-wrap w-100">
          <div class="px-1 z-future"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-03.png" class="w-100" alt=""></div>
          <div class="px-1 z-future"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-05.png" class="w-100" alt=""></div>
          <div class="px-1 z-future"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-08.png" class="w-100" alt=""></div>
        </div>
      </div>
      <div class="bg-futureU"><img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="" class="w-100">
      </div>
    </div>
  </div>
</section>

<section class="container-fluid bg-Lgray py-15">
  <div class="container">
    <div class="row">
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center bg-white">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-01.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-5 py-lg-2 p-xl-5 p-4 future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">健康経営への取り組み</p>
            <p class="future-detail">
              からだの健康、こころの健康、働きやすい環境の整備に努め、従業員の健康維持・向上につなげます。また、健康経営に取り組む企業として、培った知識を県民の皆さんに発信し、健康増進活動を広めるお手伝いをしていきます。
            </p>
            <div class="text-center houjinLogo pt-5"><img
                src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-houjinLogo.png" alt=""></div>
          </div>
          <div class="col-12 my-5">
            <div class="row no-gutters px-3">
              <div class="col-12 col-md-4 px-3 my-2">
                <div class="border balance-br py-5 h-100">
                  <p class="text-center f-22 text-blue font-noto-bold">からだの健康</p>
                  <div class="text-center balance-icon mb-2"><img
                      src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-icon-01.png" alt=""></div>
                  <div class="text-center">
                    <ul class="m-0 px-2 text-left d-inline-block b-list">
                      <li>健診受診100％の維持</li>
                      <li>要精密検査・要再検査の促進</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-4 px-3 my-2">
                <div class="border balance-br py-5 h-100">
                  <p class="text-center f-22 text-blue font-noto-bold">こころの健康</p>
                  <div class="text-center balance-icon mb-2"><img
                      src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-icon-02.png" alt=""></div>
                  <div class="text-center">
                    <ul class="m-0 px-2 text-left d-inline-block b-list">
                      <li>カウンセラーや電話による<br class="d-block d-md-none d-lg-block">カウンセリング</li>
                      <li>ストレスチェックの実施</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-4 px-3 my-2">
                <div class="border balance-br py-5 h-100">
                  <p class="text-center f-22 text-blue font-noto-bold">働きやすい環境</p>
                  <div class="text-center balance-icon mb-2"><img
                      src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-icon-03.png" alt=""></div>
                  <div class="text-center">
                    <ul class="m-0 px-2 text-left d-inline-block b-list">
                      <li>適正な労働時間の管理</li>
                      <li>積極的な休暇取得の促進</li>
                      <li>社員食堂の充実</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-02.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-5 py-lg-2 p-xl-5 p-4 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">働き方改革の推進</p>
            <p class="future-detail">
              従業員がそれぞれのライフステージに合わせ、継続的に働ける環境を整備し、新しい仕組みを柔軟に取り入れて従業員が働きやすい環境づくりをすすめます。
            </p>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-balance-03.jpg" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-5 py-lg-2 p-xl-5 p-4 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">多様な人材の活躍推進</p>
            <p class="future-detail">
              様々なパーソナリティを持った従業員がジェンダーフリーに心身共に健やかに就業し、活躍できる環境を整えます。
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


</div>
</section>

<?php
get_footer();