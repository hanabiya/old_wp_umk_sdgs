<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package umk-sdgs
 */

get_header();
?>
<div id="FV" class="container-fluid px-0 overflow-hidden">
	<div class="container">
		<div class="row position-relative">
			<div class="col-12 mv-copy d-none d-sm-block">
				<div class="d-flex justify-content-between aline-items-start">
					<div class="u-copy"><img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_copy.png" class="w-100"></div>
					<div class="u-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/umk-sdgs-logo.png" class="w-100"></div>
				</div>
			</div>
			<div class="col-12 mv-copy-sp d-block d-sm-none">
				<div class="">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk-sdgs-logo.png" class="w-100 mb-4">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_copy_sp.png" class="w-100">
				</div>
			</div>
		</div>
	</div>
	<div class="position-relative">
		<div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel" data-interval="4000" data-pause="hover">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_slider_02.jpg" class="w-100 d-none d-sm-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_slider_sp_02.jpg" class="w-100 d-block d-sm-none">
				</div>
				<div class="carousel-item">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_slider_01.jpg" class="w-100 d-none d-sm-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_slider_sp_01.jpg" class="w-100 d-block d-sm-none">
				</div>
				<div class="carousel-item">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_slider_03.jpg" class="w-100 d-none d-sm-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/umk_mv_slider_sp_03.jpg" class="w-100 d-block d-sm-none">
				</div>
			</div>
		</div>
		<div class="scrolldown"><span>SCROLL</span></div>
	</div>
</div>

<div class="container-fluid px-0">
	<div class="row">
		<div class="col-12 mv-line"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-mv-line.png" class="w-100"></div>
	</div>
</div>

<section class="container sengen pb-10">
	<div class="row no-gutters position-relative">
		<div class="bg-u-01"><img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="" class="w-100"></div>
		<div class="col-12 text-center border-blue border-bottom">
			<h2 class="front-top-title text-blue pb-5 font-noto-bold">テレビ宮崎 <br class="d-md-none">SDGs宣言</h2>
		</div>
		<div class="col-12 py-8 text-center border-blue border-bottom">
			<p class="text-left text-md-center mb-0 font-genno-medium">放送、事業を通じて地域社会の課題や地域経済の発展を図ることで持続可能な社会の実現に貢献してまいります。<br>
				何より、子どもたちが希望を抱き、夢を持ち続ける世界を作るための課題認識を持ち、<br class="d-none d-md-block">
				地域の皆さまとともに、その解決に向けた活動を「子どもの未来のために」取り組んでまいります。</p>
		</div>
		<div class="col-12 text-center pt-8 pb-10"><a href="<?php echo home_url('/'); ?>sdgs-declaration" class="bgleft d-inline-block more-btn"><span>詳細を見る</span></a></div>
	</div>
</section>

<section>
	<div class="container-fluid bg-Lgray pt-15 pb-18">
		<div class="row">
			<div class="container">
				<div class="row text-center">
					<div class="col-12 mb-8 pt-5">
						<h3 class="sec-title text-blue font-noto-bold">SDGs<br class="d-md-none">(持続可能な開発目標)とは</h3>
					</div>
					<div class="col-12 mb-5">
						<p class="text-left text-md-center">2015年に国連で採択された、<span class="font-genno-bold">「持続可能な開発⽬標 (<span class="c-red">S</span>ustainable <span class="c-red">D</span>evelopment <span class="c-red">G</span>oal<span class="c-red">s</span>)
								」</span><br class="d-none d-md-block">のことです。<br class="d-md-none">環境・衛⽣、⼈権、気候変動、教育、貧困など、世界が抱える社会問題を、17の⽬標と<br class="d-none d-md-block">169のターゲット(詳細な⽬標)に整理しています。2030年までの達成を⽬指します。</p>
					</div>
					<div class="col-12 py-7 bg-white sdgs-detail">
						<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-title-logo.png" class="w-100 pb-5">
						<div class="row px-7">
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-01.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-02.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-03.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-04.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-05.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-06.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-07.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-08.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-09.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-10.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-11.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-12.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-13.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-14.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-15.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-16.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
							<div class="p-2 col-4 col-md-2"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-18.png" class="w-100" alt=""></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="media-compact">
	<div class="container-fluid bg-mc">
		<div class="row no-gutters py-10">
			<div class="container bg-white">
				<div class="row py-10 px-6 justify-content-center no-gutters">
					<div class="col-12 col-lg-3 mb-2 text-center"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-mc-logo.png" class="w-100" alt=""></div>
					<div class="col-12 col-lg-7 pl-lg-6">
						<h3 class="mt-2 mb-5 f-18 f-md-22 text-center text-lg-left font-noto-bold">SDGメディア・コンパクト</h3>
						<p class="mb-5">テレビ宮崎は、国連「SDGメディア・コンパクト」に2021年5月、加盟しました。<br>
							SDGメディア・コンパクトは、世界中の報道機関とエンターテインメント企業が、<br class="d-none d-xl-block">SDGs達成に向け、その力を積極的に活用するための連携の枠組みです。
						</p>
						<div class="text-center text-lg-left mb-4">
							<a href="https://www.unic.or.jp/activities/economic_social_development/sustainable_development/2030agenda/sdg_media_compact/" class="d-inline-block bgleft more-btn mb-2 mb-md-0" target="_blank"><span>国連広報センター</span></a>
							<a href="https://www.un.org/sustainabledevelopment/sdg-media-compact-about/" class="d-inline-block bgleft more-btn mb-2 mb-md-0 ml-md-5" target="_blank"><span>国連本部</span></a>
						</div>
						<a href="<?php echo home_url('/') ?>promise">
							<img src="<?php echo get_template_directory_uri(); ?>/img/bnr-sdgs15.jpg" alt="" class="d-block w-100 mw-100">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid bg-Lgray py-15">
		<div class="row">
			<div class="container">
				<div class="row position-relative">
					<div class="col-12 mb-8 text-center">
						<h3 class="sec-title text-blue font-noto-bold">テレビ宮崎のSDGs<br class="d-md-none">への取り組み</h3>
					</div>
					<!-- <div class="col-12 mb-5 text-center">
						<p class="text-left text-md-center">2015年に国連で採択された、「持続可能な開発⽬標 (Sustainable Development Goals) 」のことです。<br>
						環境・衛⽣、⼈権、気候変動、教育、貧困など、世界が抱える社会問題を、17の⽬標と169のターゲット(詳細な⽬標)に整理しています。</p>
					</div> -->
					<div class="col-12 mb-10">
						<div class="row no-gutters align-items-center">
							<div class="col-12 col-lg-6 align-self-stretch hov-init">
								<div class="phot-init-frame">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-01.jpg" class="img01" alt="">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-mark-01.png" class="img0 position-absolute" alt="">
								</div>
							</div>
							<div class="col-12 col-lg-6 p-md-7 p-4 bg-white initiatives align-self-stretch">
								<p class="text-blue font-weight-bold mb-1 font-noto-bold">喜びと笑顔あふれる未来へ</p>
								<p class="" style="letter-spacing:1.3px;">「子どもたちの見つめる未来が光あるものとなるように…」<br>
									そんな願いをこめてスポーツや文化的なイベントを通じて、<br class="d-xl-block d-none">
									子どもたちの明るく健やかな成長を手助けします。</p>
								<div class="text-center text-lg-left pt-md-6">
									<a href="<?php echo home_url('/') ?>future/" class="d-inline-block bgleft more-btn">
										<span>詳細を見る</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 mb-10">
						<div class="row no-gutters align-items-center">
							<div class="col-12 col-lg-6 align-self-stretch">
								<div class="phot-init-frame">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-02.jpg" class="img01" alt="">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-mark-02.png" class="img0 position-absolute" alt="">
								</div>
							</div>
							<div class="col-12 col-lg-6 p-md-7 p-4 bg-white initiatives align-self-stretch">
								<p class="text-blue font-weight-bold mb-1 font-noto-bold">地域・社会への貢献</p>
								<p class="" style="letter-spacing:1.3px;">「伝える」・「発信する」メディアとしての強みを活かし、
									誰もが安心して住み続けられる宮崎の街づくりへ
									貢献していきます。</p>
								<div class="text-center text-lg-left pt-md-6"><a href="<?php echo home_url('/') ?>contribution/" class="d-inline-block bgleft more-btn"><span>詳細を見る</span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 mb-10">
						<div class="row no-gutters align-items-center">
							<div class="col-12 col-lg-6 align-self-stretch">
								<div class="phot-init-frame">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-03.jpg" class="img01" alt="">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-mark-03.png" class="img0 position-absolute" alt="">
								</div>
							</div>
							<div class="col-12 col-lg-6 p-md-7 p-4 bg-white initiatives align-self-stretch">
								<p class="text-blue font-weight-bold mb-1 font-noto-bold" style="line-height:1.6 !important;">ワークライフバランスの<br class="d-block d-md-none">実践と提案</p>
								<p class="" style="letter-spacing:1.3px;">私たち自身が心身共に健康で、
									一人一人がいきいきと働ける環境を整え、実践し
									メディアとして県民の皆さんに広く発信していきたいと考えています。</p>
								<div class="text-center text-lg-left pt-md-6"><a href="<?php echo home_url('/') ?>balance/" class="d-inline-block bgleft more-btn"><span>詳細を見る</span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 mb-10">
						<div class="row no-gutters align-items-center">
							<div class="col-12 col-lg-6 align-self-stretch">
								<div class="phot-init-frame">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-04.jpg" class="img01" alt="">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-Initiatives-mark-05.png" class="img0 position-absolute" alt="">
								</div>
							</div>
							<div class="col-12 col-lg-6 p-md-7 p-4 bg-white initiatives align-self-stretch">
								<p class="text-blue font-weight-bold mb-1 font-noto-bold">地域環境保全のために</p>
								<p class="" style="letter-spacing:1.3px;">私たちの一つ一つの取り組みがやがて大きな変化につながると信じ、
									人と地球が気持ち良く明日を過ごせる未来のために、
									クリーンな環境の推進を図ります。</p>
								<div class="text-center text-lg-left pt-md-6"><a href="<?php echo home_url('/') ?>conservation/" class="d-inline-block bgleft more-btn"><span>詳細を見る</span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="bg-u-02"><img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="" class="w-100"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
