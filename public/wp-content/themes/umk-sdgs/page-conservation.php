<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="container-fluid bg-mv-conservation header-m h-fv mv-img-border-bottom">
  <div class="container">
    <div class="row">
    </div>
  </div>
</section>

<section class="container-fluid mt-md-10 mb-md-10">
  <div class="container">
    <div class="row text-center position-relative">
      <div class="col-12 pt-10 pb-5">
        <h3 class="sec-title text-blue mb-8 font-noto-bold">地域環境保全のために</h3>
        <p class="text-left text-md-center">
          私たちの一つ一つの取り組みがやがて大きな変化につながると信じ、<br>
          人と地球が気持ち良く明日を過ごせる未来のために、クリーンな環境の推進を図ります。
        </p>
      </div>
      <div class="col-12 pb-10 mx-auto">
        <div class="d-flex future-mark justify-content-center flex-wrap w-100 mb-2">
          <div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-07.png" class="w-100" alt=""></div>
          <div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-12.png" class="w-100" alt=""></div>
          <div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-13.png" class="w-100" alt=""></div>
          <div class="z-future px-1 d-none d-md-block"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-15.png" class="w-100" alt=""></div>
          <div class="z-future px-1 d-none d-md-block"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
        </div>
        <div class="d-flex future-mark justify-content-center flex-wrap w-100">
          <div class="z-future px-1 d-block d-md-none"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-15.png" class="w-100" alt=""></div>
          <div class="z-future px-1 d-block d-md-none"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="container-fluid bg-Lgray py-15 position-relative">
  <div class="container">
    <div class="row">
    <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs_conservation-00.png" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">食べKID<span style="font-size: 20px;">(たべきっど)</span>宣言</p>
            <p class="future-detail">
              「食品ロス」を減らす取り組みをメディアとしての強みを活かして広く県民に発信していきます。
            </p>
            <div class="text-center text-lg-left"><a href="https://www.umk.co.jp/kid.html" class="d-inline-block more-btn" target="_blank">詳細を見る</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs_conservation-01.png" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">社屋の省エネ化</p>
            <p class="future-detail">
              スタジオの照明のLED化や制作機材の省エネ化など、地球にやさしく環境に配慮した社屋の維持に努めます。
            </p>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs_conservation-02.png" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">ペーパーレス化</p>
            <p class="future-detail">
              ペーパーレス化を進め、全社一丸となって紙の削減に取り組みます。
            </p>
          </div>
        </div>
      </div>
      <div class="col-12 mb-10">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-xl-6 col-lg-5 align-self-stretch">
            <div class="phot-future-frame">
              <img src="<?php echo get_template_directory_uri(); ?>/img/sdgs_conservation-03.png" class="" alt="">
            </div>
          </div>
          <div class="col-12 col-xl-6 col-lg-7 px-lg-7 py-lg-5 p-6 bg-white future align-self-stretch">
            <p class="text-blue font-weight-bold mb-1 font-noto-bold">ごみの分別</p>
            <p class="future-detail">
              廃棄物のリサイクルについて従業員全員で考え、実践しています。
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="pos-conservation-u">
    <img src="<?php echo get_template_directory_uri(); ?>/img/bg-u.png" alt="">
  </div>
</section>


</div>
</section>

<?php
get_footer();
