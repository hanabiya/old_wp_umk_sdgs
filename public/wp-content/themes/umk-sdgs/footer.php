<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package umk-sdgs
 */

?>

<footer id="colophon" class="site-footer">
  <div class="container site-info">
    <div class="row">
      <div class="col-12 mt-20 mb-6 text-center footer-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/img/umk-sdgs-logo.png" class="w-100"
          alt="">
			</div>
			<div class="col-12">
				<p class="f-12 text-center mb-0" style="line-height: 1.6;">Copyright © Miyazaki Telecasting Co.,ltd. All rights reserved .</p>
				<p class="text-center f-12" style="line-height: 1.6;">[注意事項] 当ホームページに掲載されている記事・画像の無断転⽤を禁⽌します。著作権はテレビ宮崎またはその情報提供者に属します。</p>
			</div>
    </div>
  </div>
	<div class="container-fluid px-0">
		<div class="row no-gutters">
			<div class="col-12"><img src="<?php echo get_template_directory_uri(); ?>/img/borderline-sp.png" class="w-100 d-block d-md-none" alt=""><img src="<?php echo get_template_directory_uri(); ?>/img/borderline.png" class="w-100 d-none d-md-block" alt=""></div>
		</div>
	</div>
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>

</html>