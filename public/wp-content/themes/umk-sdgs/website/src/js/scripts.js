import $ from 'jquery';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/carousel';
import 'bootstrap/js/dist/tab';
import Swiper from 'swiper';
import { gsap } from 'gsap';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
import { DrawSVGPlugin } from 'gsap/DrawSVGPlugin';

gsap.registerPlugin(ScrollToPlugin, DrawSVGPlugin);


const hamburgerMenuToggle = () => {
    let windowWidth = window.innerWidth;
    let hamburgerMenu = '.menu-trigger';
    let toggleMenu = '.navbar-right-toggle';
    // ハンバーガーメニュー を閉じるボタンに変更
    $(hamburgerMenu).on('click', () => {
        if ($(hamburgerMenu).hasClass('active')) {
            $(hamburgerMenu).removeClass('active');
            $('.menu-slide').removeClass('active');
        } else {
            $(hamburgerMenu).addClass('active');
            $('.menu-slide').addClass('active');
        }
    });
};

// 検索エリアのクリック開閉の挙動
const dataSearchToggle = () => {
    const trigger = document.querySelectorAll('.data-search-head');
    for (let i = 0; i < trigger.length; i++) {
        trigger[i].addEventListener('click', () => {
            const sibling = trigger[i].nextElementSibling;
            if(trigger[i].classList.contains('active')){
                trigger[i].classList.remove('active');
                sibling.classList.remove('active');
            } else {
                trigger[i].classList.add('active');
                sibling.classList.add('active');
            }
        })
    }
};

const smoothScroll = () => {
    $('.js-smooth').on('click', function (ev) {
        let navHeight = $('header').innerHeight();
        // prevent default click event
        ev.preventDefault();
        // get the scroll-to target
        let targetSection = $(this).attr('href');
        // get vertical offset of target
        let targetOffset = $(targetSection).offset().top;
        // scroll to the starget section
        gsap.to(window, 2, {
            scrollTo: { y: targetOffset },
            ease: 'power4.out',
        });
    });
};

const headerTransform = () => {
    $(window).on("scroll", function () {

        let hh = $('header').innerHeight();
        let h = $('#FV').innerHeight();
        console.log(hh);
        h = h - 50;
        if ($(window).scrollTop() > h) {
            $('header').addClass('is_set');
        } else {
            $('header').removeClass('is_set');
        }

        if ($(window).scrollTop() > hh) {
            $('header').css("top", '-' + hh + 'px');
        } else {
            $('header').css("top", "0");
        }
    })
}

$(function () {
    $('.nav li a').each(function () {
        var $href = $(this).attr('href');
        if (location.href.match($href)) {
            $(this).addClass('current');
        } else {
            $(this).removeClass('current');
        }
    });
});

$(() => {
    hamburgerMenuToggle();
    smoothScroll();
    headerTransform();
    dataSearchToggle();
});
