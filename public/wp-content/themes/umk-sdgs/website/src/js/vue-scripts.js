const cm = new Vue({
    el: '#cm',
    data: {
        baseUrl: 'https://www.umk.co.jp/sdgs-actions/wp-json/wp/v2/',
        postCM: [],
        currentCM: [],
        selected: {
            year: null,
            month: null,
        },
        current: {
            year: null,
            month: null,
        },
        years: [],
        months: {
            12: 'december',
            11: 'november',
            10: 'october',
            9: 'september',
            8: 'august',
            7: 'july',
            6: 'june',
            5: 'may',
            4: 'april',
            3: 'march',
            2: 'february',
            1: 'january',
        },
        isLoading: true,
    },
    methods: {
        getYears: function () {
            const startYear = 2022; // 年の最小値
            const endYear = new Date().getFullYear(); // 現在の年を取得
            for (let i = endYear; i >= startYear; i--) {
                this.years.push(i); // スタートの年から終了の年までを配列に格納
            }
        },
        inputYear: function (year) {
            cm.$set(this.selected, 'year', year);
        },
        inputMonth: function (month) {
            cm.$set(this.selected, 'month', month);
        },
        changeCM: async function () {
            this.isLoading = true;
            const yearUrl = `${this.baseUrl}week_year/?per_page=100`;
            const monthUrl = `${this.baseUrl}week_list/?per_page=100`;
            const year = this.selected.year;
            const month = this.selected.month;
            const yearID = await this.getYearId(year, yearUrl);
            const monthID = await this.getMonthId(month, monthUrl);
            const url = `${this.baseUrl}weekly_cm?week_year=${yearID}&week_list=${monthID}&per_page=100`;

            cm.$set(this.current, 'month', month);
            cm.$set(this.current, 'year', year);

            await fetch(url).then(res => res.json()).then(data => {
                cm.$set(this, 'postCM', data);
            }).catch(err => alert(err))
            this.isLoading = false;
        },
        getYearId: function (year, url) {
            const yearId = fetch(url)
                .then(res => res.json())
                .then(data => {
                    let id;
                    data.forEach(e => {
                        if (e.slug === String(year)) {
                            id = e.id;
                        }
                    })
                    return id;
                }).catch(err => alert(err))
            return yearId;
        },
        getMonthId: function (month, url) {
            const monthId = fetch(url)
                .then(res => res.json())
                .then(data => {
                    let id;
                    data.forEach(e => {
                        if (e.slug === this.months[month]) {
                            id = e.id;
                        }
                    })
                    return id;
                }).catch(err => alert(err))
            return monthId;
        },
        initSelected: function () {
            const nowDate = new Date();
            const year = nowDate.getFullYear();
            const month = nowDate.getMonth();

            this.selected.year = year;
            this.selected.month = month + 1;
        },
        getYoutubeId: function (url) {
            const id = url.split('=');
            return id[1]; // youtubeのURLからidを抜き出して返します。
        },
        setCM: function (post) {
            cm.$set(this, 'currentCM', post);
        }
    },
    mounted() {
        this.getYears();
        this.initSelected();
        this.changeCM();
    },
});

const archive = new Vue({
    el: '#archive',
    data: {
        baseUrl: 'https://www.umk.co.jp/sdgs-actions/wp-json/wp/v2/',
        postCM: [],
        currentCM: [],
        selected: {
            year: null,
            month: null,
        },
        current: {
            year: null,
            month: null,
        },
        years: [],
        months: {
            12: 'december',
            11: 'november',
            10: 'october',
            9: 'september',
            8: 'august',
            7: 'july',
            6: 'june',
            5: 'may',
            4: 'april',
            3: 'march',
            2: 'february',
            1: 'january',
        },
        isLoading: true,
    },
    methods: {
        getYears: function () {
            const startYear = 2022; // 年の最小値
            const endYear = new Date().getFullYear(); // 現在の年を取得
            for (let i = endYear; i >= startYear; i--) {
                this.years.push(i); // スタートの年から終了の年までを配列に格納
            }
        },
        inputYear: function (year) {
            archive.$set(this.selected, 'year', year);
        },
        inputMonth: function (month) {
            archive.$set(this.selected, 'month', month);
        },
        changeCM: async function () {
            this.isLoading = true;
            const yearUrl = `${this.baseUrl}week_year/?per_page=100`;
            const monthUrl = `${this.baseUrl}week_list/?per_page=100`;
            const year = this.selected.year;
            const month = this.selected.month;
            const yearID = await this.getYearId(year, yearUrl);
            const monthID = await this.getMonthId(month, monthUrl);
            const url = `${this.baseUrl}broadcast_archive?week_year=${yearID}&week_list=${monthID}&per_page=100&_embed`;

            archive.$set(this.current, 'month', month);
            archive.$set(this.current, 'year', year);

            await fetch(url).then(res => res.json()).then(data => {
                archive.$set(this, 'postCM', data);
                this.isLoading = false;
            }).catch(err => alert(err))
        },
        getYearId: function (year, url) {
            const yearId = fetch(url)
                .then(res => res.json())
                .then(data => {
                    let id;
                    data.forEach(e => {
                        if (e.slug === String(year)) {
                            id = e.id;
                        }
                    })
                    return id;
                }).catch(err => alert(err))
            return yearId;
        },
        getMonthId: function (month, url) {
            const monthId = fetch(url)
                .then(res => res.json())
                .then(data => {
                    let id;
                    data.forEach(e => {
                        if (e.slug === this.months[month]) {
                            id = e.id;
                        }
                    })
                    return id;
                }).catch(err => alert(err))
            return monthId;
        },
        initSelected: function () {
            const nowDate = new Date();
            const year = nowDate.getFullYear();
            const month = nowDate.getMonth();

            this.selected.year = year;
            this.selected.month = month + 1;
        },
        getYoutubeId: function (url) {
            if (url === undefined || url === null) return;

            const id = url.split('=');
            return id[1]; // youtubeのURLからidを抜き出して返します。
        },
        setCM: function (post) {
            archive.$set(this, 'currentCM', post);
        }
    },
    mounted() {
        this.getYears();
        this.initSelected();
        this.changeCM();
    },
});
