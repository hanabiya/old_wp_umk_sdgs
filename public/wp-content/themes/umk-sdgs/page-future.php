<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package umk-sdgs
 */

get_header();
?>

<section id="FV" class="container-fluid bg-mv-future header-m h-fv mv-img-border-bottom">
	<div class="container">
		<div class="row">
		</div>
	</div>
</section>

<section class="container-fluid mt-md-10 mb-md-10">
	<div class="container">
		<div class="row text-center position-relative">
			<div class="col-12 pt-10 pb-5 z-10">
				<h3 class="sec-title text-blue mb-8 font-noto-bold">喜びと笑顔あふれる未来へ</h3>
				<p class="text-left text-md-center fuchidori">「子どもたちの見つめる未来が光あるものとなるように…」そんな願いをこめて、<br>
					スポーツや文化的なイベントを通じて、子どもたちの明るく健やかな成長を手助けします。
				</p>
			</div>
			<div class="col-12 pb-10 mx-auto">
				<div class="d-flex future-mark justify-content-center flex-wrap w-100 mb-2">
					<div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-01.png" class="w-100" alt=""></div>
					<div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-03.png" class="w-100" alt=""></div>
					<div class="z-future px-1"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-04.png" class="w-100" alt=""></div>
					<div class="z-future px-1 d-none d-md-block"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-10.png" class="w-100" alt=""></div>
					<div class="z-future px-1 d-none d-md-block"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
				</div>
				<div class="d-flex future-mark justify-content-center flex-wrap w-100">
					<div class="z-future px-1 d-block d-md-none"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-10.png" class="w-100" alt=""></div>
					<div class="z-future px-1 d-block d-md-none"><img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-detail-17.png" class="w-100" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="container-fluid bg-Lgray py-15">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-10">
				<div class="row no-gutters align-items-center">
					<div class="col-12 col-lg-6 align-self-stretch">
						<div class="phot-future-frame">
							<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-future-01.jpg" class="img01" alt="">
						</div>
					</div>
					<div class="col-12 col-lg-6 px-lg-7 py-lg-3 py-xl-5 py p-6 bg-white future align-self-stretch">
						<p class="text-blue font-weight-bold mb-1 font-noto-bold">こども未来応援プロジェクト</p>
						<p class="future-detail">
							「喜びや感動などスポーツの持つ力で、子どもたちがより良く成長できる機会を創出し、子どもの社会性や協調性、自立心や自ら考える力を育む活動を継続してまいります。また子どもを通じて、全世代にも栄養摂取および運動習慣の意識付けを行い、健康維持、増進につながるようサステナブルな社会の実現に貢献することを目標とします。
						</p>
						<div class="text-center text-lg-left">
							<a href="https://www.umk-kodomo-mirai.com/" class="d-inline-block more-btn-future bgleft" target="_blank"><span>詳細を見る</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 mb-10">
				<div class="row no-gutters align-items-center">
					<div class="col-12 col-lg-6 align-self-stretch">
						<div class="phot-future-frame">
							<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-future-hinata.jpg" class="img01" alt="">
						</div>
					</div>
					<div class="col-12 col-lg-6 px-lg-7 py-lg-3 py-xl-5 py p-6 bg-white future align-self-stretch">
						<p class="text-blue font-weight-bold mb-1 font-noto-bold">ひなた探究</p>
						<p class="future-detail">
							探究学習（総合的な探究の時間）は、2022年4月から全国の高等学校において必修化されました。<br>
							UMKでは、高校生に対して地元企業の「自社で抱える課題」や「取り組んでいる社会課題」を掲載し、
							高校生自身の「暮らしとつながる学習」と「キャリア選択」をサポートしています。
						</p>
						<div class="text-center text-lg-left">
							<a href="https://www.umk.co.jp/hinata-tankyu.html" class="d-inline-block more-btn-future bgleft" target="_blank"><span>詳細を見る</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 mb-10">
				<div class="row no-gutters align-items-center">
					<div class="col-12 col-lg-6 align-self-stretch">
						<div class="phot-future-frame">
							<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-future-furugi.jpg" class="img01" alt="">
						</div>
					</div>
					<div class="col-12 col-lg-6 px-lg-7 py-lg-3 py-xl-5 py p-6 bg-white future align-self-stretch">
						<p class="text-blue font-weight-bold mb-1 font-noto-bold">古着deワクチン</p>
						<p class="future-detail">
							不要になった衣類等を手放すことで誰かの役に立ち、世界の子どもたちにワクチンを届けることができます。<br>
							毎月１回、社内で不要な衣類や雑貨などを集め、寄付をする活動を行っています。
						</p>
					</div>
				</div>
			</div>
			<div class="col-12 mb-10">
				<div class="row no-gutters align-items-center">
					<div class="col-12 col-lg-6 align-self-stretch">
						<div class="phot-future-frame">
							<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-future-03.jpg" class="img01" alt="">
						</div>
					</div>
					<div class="col-12 col-lg-6 px-lg-7 py-lg-3 py-xl-5 py p-6 bg-white future align-self-stretch">
						<p class="text-blue font-weight-bold mb-1 font-noto-bold">高校生フォーラム</p>
						<p class="future-detail">
							各分野で活躍する宮崎県出身者をパネリストとして招き高校生が将来の夢や働くことについて、参加者全員で考え、話し合います。多様化する社会の中で、新しい時代を切り拓くヒントを見つけ出し、将来の夢の実現をサポートします。
						</p>
					</div>
				</div>
			</div>
			<div class="col-12 mb-10">
				<div class="row no-gutters align-items-center">
					<div class="col-12 col-lg-6 align-self-stretch">
						<div class="phot-future-frame">
							<img src="<?php echo get_template_directory_uri(); ?>/img/sdgs-future-02.jpg" class="img01" alt="">
						</div>
					</div>
					<div class="col-12 col-lg-6 px-lg-7 py-lg-3 py-xl-5 py p-6 bg-white future align-self-stretch">
						<p class="text-blue font-weight-bold mb-1 font-noto-bold">ゆっぴーチャンネル　KIDS&UMK</p>
						<p class="future-detail mb-2 mb-xl-4">
							UMKアナウンサーによる読み聞かせを配信し、子どもたちに物語の世界を楽しんでもらおうというアナウンサーコンテンツ。新型コロナの影響で休校を余儀なくされた２０２０年４月からスタート、２０２１年４月からは読み聞かせ専門チャンネルとして子育て世代に届けていきます。
						</p>
						<div class="text-center text-lg-left"><a href="https://www.youtube.com/channel/UC2T-H9VXDreri3ryWHFG6Pw" class="d-inline-block more-btn-future bgleft" target="_blank"><span>詳細を見る</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


</div>
</section>

<?php
get_footer();
