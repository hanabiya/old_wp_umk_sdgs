#! /bin/sh

apt-get update -y

# SSH接続でパスワード利用の場合
apt-get install sshpass

# SSH接続で公開鍵接続も認証パスが必要になる場合
apt-get install -y expect


eval `ssh-agent`

expect -c "
set timeout 3
spawn ssh-add /home/.ssh/xserver/id_rsa
expect \"Enter passphrase for /home/.ssh/xserver/id_rsa:\"
interact
"

/bin/bash
