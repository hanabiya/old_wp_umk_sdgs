# wordpress-docker + phpmyadmin + wp-cli + wordmove
DockerによるWordpressのローカル環境構築を行うためのリポジトリ

以下参考:

[Quick-start-wordpress-docker - Github](https://github.com/kawamataryo/quick-start-wordpress-docker)

[WP-CLI ＆ Dockerで「爆速」WordPress環境構築](https://it-web-life.com/wpcli_docker_wordpress/)

[DOCKER-COMPOSEでWORDPRESS開発環境を作る＋自動化](https://taak.biz/archives/3942)

# Features
* DockerによるWordpressのローカル環境構築
* WP-CLIによるWordpressの自動化
* wordmoveへの本番環境デプロイ, バックアップ

# How to start

### 0. 事前準備
Macにdocker, docker-composeをインストール  
以下参考:
[Docker Compose - インストール - Qiita](https://qiita.com/zembutsu/items/dd2209a663cae37dfa81)

### 1. .env(環境変数)の設定
.envにdocker-composeで使う環境変数を定義しています。  
コメントを元に記載してください。

```
# -------------------------------------------
# wordpress・mysqlコンテナの設定
# -------------------------------------------
# プロダクトの名前 作成されるcontainer名の接頭語として使用
PRODUCTION_NAME=kittymiyagyu
# local wordpressを紐付けるPort名（ex: 8080)
LOCAL_SERVER_PORT=8080
# localのmysqlを紐付けるPort名（ex: 3306)
LOCAL_DB_PORT=3306

# -------------------------------------------
# wordmoveコンテナの設定
# -------------------------------------------
# 全て本番環境の情報
# URL
PRODUCTION_URL=https://miyazakigyu-hellokitty2021.com
# wordpressのディレクトリの絶対パス
PRODUCTION_DIR_PATH=/home/fujifac00002/miyazakigyu-hellokitty2021.com/public_html
# DB名
PRODUCTION_DB_NAME=fujifac00002_kitty
# DBのユーザー名
PRODUCTION_DB_USER=fujifac00002_zom
# DBのパスワード
PRODUCTION_DB_PASSWORD=4GWdB49xXwy2
# DBのホスト名
PRODUCTION_DB_HOST=localhost
# DBの接続ポート
PRODUCTION_DB_PORT=3306
# SSHホスト名
PRODUCTION_SSH_HOST=sv12755.xserver.jp
# SSHユーザー名
PRODUCTION_SSH_USER=fujifac00002
# SSHポート名
PRODUCTION_SSH_PORT=10022

# -------------------------------------------
# テスト環境の情報
# -------------------------------------------
# URL
STAGING_URL=http://fujifac00002.xsrv.jp/kittymiyagyu_staging
# wordpressのディレクトリの絶対パス
STAGING_DIR_PATH=/home/fujifac00002/fujifac00002.xsrv.jp/public_html/kittymiyagyu_staging
# DB名
STAGING_DB_NAME=fujifac00002_kittystaging
# DBのユーザー名
STAGING_DB_USER=fujifac00002_zom
# DBのパスワード
STAGING_DB_PASSWORD=4GWdB49xXwy2
# DBのホスト名
STAGING_DB_HOST=localhost
# DBの接続ポート
STAGING_DB_PORT=3306
# SSHホスト名
STAGING_SSH_HOST=sv12755.xserver.jp
# SSHユーザー名
STAGING_SSH_USER=fujifac00002
# SSHポート名
STAGING_SSH_PORT=10022
```

### 2. WP-CLIの設定
WP-CLIでのセットアップを行うためにwp-install.shを編集します。

[WP-CLI公式サイト](https://wp-cli.org/ja/)

```
#!/bin/bash

# WordPressセットアップ admin_user,admin_passwordは管理画面のログインID,PW
wp core install \
--url='http://localhost:8080' \
--title='サイトのタイトル' \
--admin_user='管理者ユーザー名' \
--admin_password='管理者パスワード' \
--admin_email='管理者メールアドレス' \
--allow-root

# 日本語化
wp language core install ja --activate --allow-root

# タイムゾーンと日時表記
wp option update timezone_string 'Asia/Tokyo' --allow-root
wp option update date_format 'Y-m-d' --allow-root
wp option update time_format 'H:i' --allow-root

# キャッチフレーズの設定 (空にする)
wp option update blogdescription '' --allow-root

# プラグインの削除 (不要な初期プラグインを削除)
wp plugin delete hello.php --allow-root
wp plugin delete akismet --allow-root

# プラグインのインストール (必要に応じてコメントアウトを外す)
wp plugin install logbook --activate --allow-root
wp plugin install advanced-custom-fields --activate --allow-root
wp plugin install custom-post-type-ui --activate --allow-root
wp plugin install custom-post-type-permalinks --activate --allow-root
wp plugin install show-current-template --activate --allow-root
wp plugin install wordpress-seo --activate --allow-root
wp plugin install wp-multibyte-patch --activate --allow-root
wp plugin install mw-wp-form --activate --allow-root

# テーマの削除
wp theme delete twentysixteen --allow-root
wp theme delete twentyseventeen --allow-root
wp theme delete twentynineteen --allow-root
# wp theme delete twentytwenty --allow-root

# テーマの追加[_s-underscoresブランクテーマ]
wp scaffold _s テーマ名[半角英数字] --theme_name="プロジェクト名" --author="HANABIYA" --sassify --activate --allow-root

# パーマリンク更新
wp option update permalink_structure /%post_id%/ --allow-root

```

### 3.Wordpressのインストール
【WordPressのコンテナ名】は
${PRODUCTION_NAME}_wp です。
${PRODUCTION_NAME}の箇所は、.envで設定したプロダクトの名前になります。

```
$ docker-compose up -d
$ docker exec -it 【WordPressのコンテナ名】 /bin/bash
$ chmod +x /tmp/wp-install.sh
$ /tmp/wp-install.sh
```

### 4. Dockerコンテナの停止
docker-composeで関連コンテナを停止します。

参考：  
docker-compose downを行うと、DBのデータも初期化されてしまうので、注意。  
もし永続化したい場合は以下を参考に設定を追加する。  
[DockerでMySQLのデータを保存する方法
](https://qiita.com/TakashiOshikawa/items/11316ffd2146b36b0d7d)

```
$ docker-compose stop
```
# How to use wordmove ?
wordmoveを使えば容易に本番環境とローカル環境の同期が可能です。

### 0. wordmove用の設定(初回のみ)
接続先サーバーとのssh接続設定、ローカルのssh/configの設定は終わっていることを前提としてます。

####0-1. wpmove.shの編集
wordmoveのコンテナ名を書き換えます。
wordmoveのコンテナ名は、
${PRODUCTION_NAME}_wordmove
です。
${PRODUCTION_NAME}の箇所は、.envで設定したプロダクトの名前になります。

```
#!/bin/sh

set -eu

echo "Preparing Wordmove --------------------------------"
docker exec -w /home/ -it [ wordmoveのコンテナ名 ] /home/setup.sh

```

####0-2. config/setup.shの編集

```
#! /bin/sh

apt-get update -y

# SSH接続でパスワード利用の場合
apt-get install sshpass

# SSH接続で公開鍵接続も認証パスが必要になる場合
apt-get install -y expect


eval `ssh-agent`

expect -c "
set timeout 3
spawn ssh-add /home/.ssh/[ 接続先サーバーに合わせてid_rsaのパスを記載(ex.xserver/id_rsa ]
expect \"Enter passphrase for /home/.ssh/[ 接続先サーバーに合わせてid_rsaのパスを記載(ex.xserver/id_rsa ]:\"
interact
"

/bin/bash

```


### 1. wordmoveのコンテナに接続 + ssh接続
以下コマンドで、wordmove実行環境に入れます。

```
$ ./wpmove.sh
```

### 2. 同期・デプロイ
あとはコマンド一発で同期が可能です。
wordmoveコマンドで、ステージング環境or本番環境を切り替えて利用します。

```
wordmove [push / pull] -e [production / staging] -[オプション]
```

ステージング環境からpull
```
$ wordmove pull -e staging --all
```

ステージング環境へpush
```
$ wordmove push -e staging --all
```

本番環境からpull
```
$ wordmove pull -e production --all
```

本番環境へpush
```
$ wordmove push -e production --all
```

さらにオプション部分の指定で、DBのみthemesのみなど同期内容を変更可能です。  
参考：  
[Wordmoveの基本操作 - Qiita](https://qiita.com/mrymmh/items/c644934cac386d95b7df)


# TODO
* MySQLのデータ永続化の設定追加
