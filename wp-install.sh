#!/bin/bash

# WordPressセットアップ admin_user,admin_passwordは管理画面のログインID,PW
wp core install \
--url='http://localhost:8080' \
--title='サイトのタイトル' \
--admin_user='hanabiya_admin' \
--admin_password='FEUkxQXjazsH' \
--admin_email='web@hanabi-ya.jp' \
--allow-root

# 日本語化
wp language core install ja --activate --allow-root

# タイムゾーンと日時表記
wp option update timezone_string 'Asia/Tokyo' --allow-root
wp option update date_format 'Y-m-d' --allow-root
wp option update time_format 'H:i' --allow-root

# キャッチフレーズの設定 (空にする)
wp option update blogdescription '' --allow-root

# プラグインの削除 (不要な初期プラグインを削除)
wp plugin delete hello.php --allow-root
wp plugin delete akismet --allow-root

# プラグインのインストール (必要に応じてコメントアウトを外す)
wp plugin install logbook --activate --allow-root
wp plugin install advanced-custom-fields --activate --allow-root
wp plugin install custom-post-type-ui --activate --allow-root
wp plugin install custom-post-type-permalinks --activate --allow-root
wp plugin install show-current-template --activate --allow-root
wp plugin install wordpress-seo --activate --allow-root
wp plugin install wp-multibyte-patch --activate --allow-root
wp plugin install mw-wp-form --activate --allow-root

# テーマの削除
wp theme delete twentysixteen --allow-root
wp theme delete twentyseventeen --allow-root
wp theme delete twentynineteen --allow-root
# wp theme delete twentytwenty --allow-root

# テーマの追加[_s-underscoresブランクテーマ]
wp scaffold _s テーマ名[半角英数字] --theme_name="プロジェクト名" --author="HANABIYA" --sassify --activate --allow-root

# パーマリンク更新
wp option update permalink_structure /%post_id%/ --allow-root
